#include <iostream>
#include <conio.h>

using namespace std;

int silnia(int liczba){
	int wynik = 1;

	for(int i=1; i<=liczba; i++){
		wynik *=i;
	}

	return wynik;
}

int obliczWynik(int n, int k){
	int wynik = 0;
	
	int nSilnia = silnia(n);
	int kSilnia = silnia(k);
	int nMinkusk = silnia(n-k);

	wynik = nSilnia/(kSilnia*nMinkusk);

	return wynik;
}

int main(){
	
	int n, m, k;

	cout << "Podaj n: " << endl;
	
	cin >> n;

	cout << "Podaj k: " << endl;

	cin >> k;	

	if( n - k > 0){
		m  = obliczWynik(n,k);
	}


	cout << "Wynik: " << m << endl;
	getch();
}