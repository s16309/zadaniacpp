#include <iostream>
#include <conio.h>
#include <string>
#include <cstdlib>
#include <math.h>

using namespace std;

void wyswietl_tablice(int tab[8][10]){

	for(int i=0; i<8; i++){
		for(int j=0; j<10; j++){
			cout << tab[i][j] << " ";
		}
		cout << endl;
	}
}


int main()
{
	int A[8][10];
	int temp_row[10];
	int l,k;

	//uzupelnianie losowymi liczbami
	for(int i=0; i<8; i++){
		for(int j=0; j<10; j++){
			A[i][j] = rand();
		}
	}

	wyswietl_tablice(A);

	cout << "Podaj l:" << endl;
	cin >> l;
	cout << "Podaj k:" << endl;
	cin >> k;

	for(int j=0; j<10; j++){
		temp_row[j] = A[k][j];
		A[k][j] = A[l][j];
		A[l][j] = temp_row[j];
	}


	wyswietl_tablice(A);
	

	

	getch();
}