#include <iostream>
#include <conio.h>

#include <math.h>
using namespace std;

int znajdzDzielniki(int liczba) {

	int suma = 0;

	for(int i =1; i < liczba; i++){
		if(liczba % i == 0){
			cout << "dzielnik: " << i << endl;
			suma += i;
		}
	}

	return suma;
}


int main()
{
	bool dzialanie = true;

	cout << "Aby zakonczyc program wpisz 0 " << endl;

    while(dzialanie){
		int n;
		int suma;

		cout << "Podaj n: " << endl;
		cin >> n;
		if(n != 0){
			suma = znajdzDzielniki(n);

			if(suma == n){
				cout << "n jest liczba doskonala";
			} else {
				cout << "n nie jest liczba doskonala";
			}
		} else {
			dzialanie = false;
		}

	}
}